// Fill out your copyright notice in the Description page of Project Settings.

#include "CrosshairHUD.h"

#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"

/**
 * Function : DrawHUD
 */
void ACrosshairHUD::DrawHUD()
{
	Super::DrawHUD();

	if (mCrosshairTexture)
	{
		FVector2D center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);
		FVector2D crossHairDrawPosition(center.X - (mCrosshairTexture->GetSurfaceWidth() * 0.5f), center.Y - (mCrosshairTexture->GetSurfaceHeight() * 0.5f));

		FCanvasTileItem tileItem(crossHairDrawPosition, mCrosshairTexture->Resource, FLinearColor::White);
		tileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(tileItem);
	}
}

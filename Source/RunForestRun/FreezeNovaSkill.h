#pragma once

#include "Skill.h"

class FreezeNovaSkill : public ISkill
{
public:
	FreezeNovaSkill(const FName& name, const float cost);

	virtual ~FreezeNovaSkill() { }

	virtual void Use(UWorld* world, class ACharacter* speller) override;
};


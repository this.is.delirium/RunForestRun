// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WayPoint.generated.h"

UCLASS()
class RUNFORESTRUN_API AWayPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWayPoint();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void InteractWithCharacter(class AActor* otherActor);
	//
	virtual void InteractWithCharacter_Implementation(class AActor* otherActor);

	// Returns the collision component.
	UFUNCTION(BlueprintCallable)
	class USphereComponent* GetCollisionComponent() { return mCollisionComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	// Collision sphere.
	UPROPERTY(VisibleAnywhere)
	class USphereComponent* mCollisionComponent;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EffectComponent.h"
#include "SpeedBuffComponent.generated.h"

/**
 * 
 */
UCLASS()
class RUNFORESTRUN_API USpeedBuffComponent : public UEffectComponent
{
	GENERATED_BODY()

public:
	USpeedBuffComponent();

	~USpeedBuffComponent();

	UFUNCTION(BlueprintCallable)
	void SetSpeedFactor(const float speedFactor) { mSpeedFactor = speedFactor; }

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	float mSpeedFactor = 1.0f;
};

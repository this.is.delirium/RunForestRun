// Fill out your copyright notice in the Description page of Project Settings.

#include "ControlElement.h"
#include "RunForestRun.h"
#include "MainCharacter.h"

// Sets default values
AControlElement::AControlElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CreateCollisionComponent();
	CreateProjectileMovementComponent();
	CreateStaticMeshComponent();

	RootComponent = mCollisionComponent;
	mStaticMeshComponent->SetupAttachment(mCollisionComponent);
}

/**
 * Function : CreateCollisionComponent
 */
void AControlElement::CreateCollisionComponent()
{
	mCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	mCollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("ControlElement"));
	mCollisionComponent->InitSphereRadius(30.0f);
	/*mCollisionComponent->bTraceComplexOnMove = true;

	mCollisionComponent->BodyInstance.bNotifyRigidBodyCollision = true;
	mCollisionComponent->BodyInstance.bUseCCD = true;*/
}

/**
 * Function : CreateProjectileMovementComponent
 */
void AControlElement::CreateProjectileMovementComponent()
{
	mProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	mProjectileMovementComponent->SetUpdatedComponent(mStaticMeshComponent);
	mProjectileMovementComponent->InitialSpeed = 0.0f;
	mProjectileMovementComponent->MaxSpeed = 1000.0f;
	mProjectileMovementComponent->bRotationFollowsVelocity = true;
	mProjectileMovementComponent->bShouldBounce = true;
	mProjectileMovementComponent->Bounciness = 0.3f;
}

/**
 * Function : CreateStaticMeshComponent
 */
void AControlElement::CreateStaticMeshComponent()
{
	mStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/BasicShapes/Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		mStaticMeshComponent->SetStaticMesh(SphereVisualAsset.Object);
		mStaticMeshComponent->SetWorldScale3D(FVector(0.5f));
	}
}

// Called when the game starts or when spawned
void AControlElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AControlElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (HasCharacterOwner())
	//	UpdatePositionUsingOwner();
}

/**
 * Function : SetCharacterOwner
 */
void AControlElement::SetCharacterOwner(ACharacter* characterOwner)
{
	mCharacterOwner = characterOwner;

	if (characterOwner)
		mProjectileMovementComponent->Deactivate();
}

/**
 * Function : UpdatePositionUsingOwner
 */
/*void AControlElement::UpdatePositionUsingOwner()
{
	AMainCharacter* mainCharacter = Cast<AMainCharacter>(GetCharacterOwner());
	if (mainCharacter)
	{
		mStaticMeshComponent->SetWorldTransform(mainCharacter->GetProjectileTransform());
	}
}*/

/**
 * Function : Throw
 */
void AControlElement::Throw(const FVector& direction)
{
	mProjectileMovementComponent->Velocity = direction * mProjectileMovementComponent->MaxSpeed;
}

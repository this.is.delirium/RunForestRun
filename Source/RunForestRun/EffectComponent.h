// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EffectComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RUNFORESTRUN_API UEffectComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEffectComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Sets the owner.
	virtual void SetCharacterOwner(class ACharacter* owner) { mCharacterOwner = owner; }

	// Returns the owner.
	class ACharacter* GetCharacterOwner() { return mCharacterOwner; }

protected:
	// Owner that receives the effect.
	UPROPERTY(VisibleDefaultsOnly)
	class ACharacter* mCharacterOwner;

	FTimerHandle mTimerHandle;
};

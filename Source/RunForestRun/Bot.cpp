// Fill out your copyright notice in the Description page of Project Settings.

#include "Bot.h"
#include "BotAIController.h"

#include "ControlElement.h"
#include "MainCharacter.h"

#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ABot::ABot()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIControllerClass = ABotAIController::StaticClass();
	Enable(true);
}

// Called when the game starts or when spawned
void ABot::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABot::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/**
 * Function : Enable
 */
void ABot::Enable(const bool enable)
{
	//AIControllerClass = enable ? ABotAIController::StaticClass() : nullptr;
	if (enable)
		GetMovementComponent()->Activate();
	else
		GetMovementComponent()->Deactivate();
}

/**
 * Function : GiveControlElementToPlayer
 */
void ABot::GiveControlElementToPlayer(AMainCharacter* character)
{
	character->Pickup(GetControlElement());
	mControlElement = nullptr;
}

/**
 * Function : PickupControlElement
 */
void ABot::PickupControlElement(AControlElement* controlElement)
{
	FAttachmentTransformRules transformRules(EAttachmentRule::KeepRelative, true);
	controlElement->SetActorRelativeLocation(FVector(100.0f, 0.0f, 0.0f));
	controlElement->AttachToActor(this, transformRules);
	controlElement->SetCharacterOwner(this);
	mControlElement = controlElement;
}

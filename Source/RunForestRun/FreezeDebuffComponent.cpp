// Fill out your copyright notice in the Description page of Project Settings.

#include "FreezeDebuffComponent.h"
#include "RunForestRun.h"

/**
 * Function : UFreezeDebuffComponent
 */
UFreezeDebuffComponent::UFreezeDebuffComponent()
{
	mFreezeTime		= 3.0f;
	mCounterTime	= 0.0f;
	mIsFinished		= false;
}

/**
 * Function : BeginPlay
 */
void UFreezeDebuffComponent::BeginPlay()
{
	Super::BeginPlay();
	GetCharacterOwner()->GetMovementComponent()->Deactivate();
}

/**
 * Function : TickComponent
 */
void UFreezeDebuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!mIsFinished)
	{
		mCounterTime += DeltaTime;
		if (mCounterTime >= mFreezeTime)
		{
			GetCharacterOwner()->GetMovementComponent()->Activate();
			mIsFinished = true;
			UnregisterComponent();
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Skill.h"

/**
 * 
 */
class RUNFORESTRUN_API NodeSkill
{
public:
	NodeSkill(NodeSkill* parent, ISkill* skill);
	~NodeSkill();
	
	//void AddChildSkill(ISkill* skill);
	ISkill* Skill() { return mSkill.Get(); }
protected:
	TSharedPtr<ISkill>	mSkill;

	//NodeSkill*					mParent;
	//TArray<NodeSkill*>			mChildren;
};

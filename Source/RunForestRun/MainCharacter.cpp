// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"
#include "RunForestRun.h"
#include "ControlElement.h"
#include "RecoveryComponent.h"
#include "TreeSkillComponent.h"

#include "FreezeNovaSkill.h"

#include "Blueprint/UserWidget.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CreateFpsCamera();
	CreatePickedupMesh();
	//CreateFpsArms();
	//GetMesh()->SetOwnerNoSee(false);

	//SpawnControlElement();

	CreateRecoveryComponents();
	CreateTestSkillTree();
}

/**
 * Function : CreateFpsCamera
 */
void AMainCharacter::CreateFpsCamera()
{
	mFpsCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	mFpsCameraComponent->SetupAttachment(GetCapsuleComponent());
	mFpsCameraComponent->bUsePawnControlRotation = true;
}

/**
 * Function : CreateFpsArms
 */
void AMainCharacter::CreateFpsArms()
{
	mArmsSkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonArms"));
	mArmsSkeletalMeshComponent->SetOnlyOwnerSee(true);
	mArmsSkeletalMeshComponent->SetupAttachment(mFpsCameraComponent);
	mArmsSkeletalMeshComponent->bCastDynamicShadow = false;
	mArmsSkeletalMeshComponent->CastShadow = false;
}

/**
 * Function : CreatePickedupMesh
 */
void AMainCharacter::CreatePickedupMesh()
{
	mPickedupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/BasicShapes/Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		mPickedupMesh->SetStaticMesh(SphereVisualAsset.Object);
		mPickedupMesh->SetWorldScale3D(FVector(0.5f));
	}

	mPickedupMesh->SetupAttachment(GetCapsuleComponent());
}

/**
 * Function : CreateRecoveryComponents
 */
void AMainCharacter::CreateRecoveryComponents()
{
	// Mana component.
	{
		mManaComponent = CreateDefaultSubobject<URecoveryComponent>(TEXT("ManaComp"));
		mManaComponent->SetMaxValue(150.0f);
		mManaComponent->SetCurrentValue(50.0f);
		mManaComponent->SetRecoveryCooldown(0.5f);
		mManaComponent->SetRecoveryValue(3.0f);

		AddOwnedComponent(mManaComponent);
	}

	// Health component.
	{

	}

	// Stamina component.
	{

	}
}

/**
 * Function : CreateTestSkillTree
 */
void AMainCharacter::CreateTestSkillTree()
{
	mTreeSkillComponent = CreateDefaultSubobject<UTreeSkillComponent>(TEXT("TreeSkill"));
	mTreeSkillComponent->AddSkill(new NodeSkill(nullptr, new FreezeNovaSkill(TEXT("Freeze Nova"), 60.0f)));
}

/**
 * Function : CreateThrowHintWidget
 */
void AMainCharacter::CreateThrowHintWidget()
{
	if (mThrowHintClass)
	{
		mThrowHintWidget = CreateWidget<UUserWidget>(GetWorld(), mThrowHintClass);
		if (mThrowHintWidget)
			mThrowHintWidget->AddToViewport();
	}
}

/**
 * Function : CreatePickupHintWidget
 */
void AMainCharacter::CreatePickupHintWidget()
{
	if (mPickupHintClass)
	{
		mPickupHintWidget = CreateWidget<UUserWidget>(GetWorld(), mPickupHintClass);
		if (mPickupHintWidget)
		{
			mPickupHintWidget->AddToViewport();
			mPickupHintWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

/**
 * Function : CreateUserInterfaceWidget
 */
void AMainCharacter::CreateUserInterfaceWidget()
{
	if (mUserInterfaceClass)
	{
		mUserInterfaceWidget = CreateWidget<UUserWidget>(GetWorld(), mUserInterfaceClass);
		if (mUserInterfaceWidget)
			mUserInterfaceWidget->AddToViewport();
	}
}

/**
 * Function : SpawnControlElement
 */
void AMainCharacter::SpawnControlElement()
{
	FRotator rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.Instigator = Instigator;

	mControlElement = GetWorld()->SpawnActor<AControlElement>(GetCapsuleComponent()->GetComponentTransform().GetLocation(), rotation, spawnParams);
	mControlElement->SetCharacterOwner(this);

	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("spawned"));
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	//SpawnControlElement();
	CreateThrowHintWidget();
	CreatePickupHintWidget();
	CreateUserInterfaceWidget();

	Reset();
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasControlElement())
	{
		UpdateControlElementPosition();
		mManaComponent->EnableRecovery(true);
	}
	else
	{
		AControlElement* controlElement = RayCastToControlElement();

		SetPickupHintVisibility(controlElement != nullptr);
		mManaComponent->EnableRecovery(false);
	}
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up movement axis.
	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &AMainCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AMainCharacter::ControlPitchInput);

	// Set up interaction actions.
	PlayerInputComponent->BindAction("Pickup", EInputEvent::IE_Pressed, this, &AMainCharacter::InputPickup);
	PlayerInputComponent->BindAction("Throw", EInputEvent::IE_Pressed, this, &AMainCharacter::Throw);

	PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AMainCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &AMainCharacter::StopJump);

	PlayerInputComponent->BindAction("Shift", EInputEvent::IE_Pressed, this, &AMainCharacter::PressShift);
	PlayerInputComponent->BindAction("Shift", EInputEvent::IE_Released, this, &AMainCharacter::ReleaseShift);

	PlayerInputComponent->BindAction("UseNovaSkill", EInputEvent::IE_Pressed, this, &AMainCharacter::PressNovaSkill);
	PlayerInputComponent->BindAction("UseNovaSkill", EInputEvent::IE_Released, this, &AMainCharacter::ReleaseNovaSkill);
}

/**
 * Function : GetForwardDirection
 */
FVector AMainCharacter::GetForwardDirection() const
{
	return FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
}

/**
 * Function : GetProjectileTransform
 */
FTransform AMainCharacter::GetProjectileTransform() const
{
	FVector cameraLocation;
	FRotator cameraRotation;
	GetActorEyesViewPoint(cameraLocation, cameraRotation);

	// Transform MuzzleOffset from camera space to world space.
	FVector muzzleLocation = cameraLocation + FTransform(cameraRotation).TransformVector(mControlElementOffset);
	FRotator muzzleRotation = cameraRotation;
	muzzleRotation.Pitch += 30.0f; // Skew the aim to be slightly upwards.

	return FTransform(muzzleRotation, muzzleLocation, FVector(0.5f));
}

/**
 * Function : GetControlElement
 */
AControlElement* AMainCharacter::GetControlElement()
{
	// return mControlElement;

	// Hack.
	TArray<AActor*> controlElements;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AControlElement::StaticClass(), controlElements);

	return (controlElements.Num() > 0) ? Cast<AControlElement>(controlElements[0]) : nullptr;
}

/**
 * Function : DetachControlElement
 */
void AMainCharacter::DetachControlElement()
{
	//mControlElement->SetCharacterOwner(nullptr);
	//mControlElement = nullptr;

	// Dirty hack: We make the additional static mesh is invisible.
	mIsPickedup = false;
	mPickedupMesh->SetVisibility(false);
}

/**
 * Function : MoveForward
 */
void AMainCharacter::MoveForward(float value)
{
	value *= mPressedShift ? 2.5f : 1.0f;
	AddMovementInput(GetForwardDirection(), value);
}

/**
 * Function : MoveRight
 */
void AMainCharacter::MoveRight(float value)
{
	FVector direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(direction, value);
}

/**
 * Function : ControlPitchInput
 */
void AMainCharacter::ControlPitchInput(float value)
{
	// Inverse Y-rotation.
	AddControllerPitchInput(-value);
}

/**
 * Function : InputPickup
 */
void AMainCharacter::InputPickup()
{
	AControlElement* controlElement = RayCastToControlElement();

	if (controlElement == nullptr)
		return;

	Pickup(controlElement);
}

/**
 * Function : Pickup
 */
void AMainCharacter::Pickup(class AControlElement* controlElement)
{
	GetWorld()->DestroyActor(controlElement);

	// Dirty hack: We make the additional static mesh is visible.
	mIsPickedup = true;
	mPickedupMesh->SetVisibility(true);

	SetEnableMovement(false);
	SetPickupHintVisibility(false);
	SetThrowHintVisibility(true);
}

/**
 * Function : Throw
 */
void AMainCharacter::Throw()
{
	if (HasControlElement())
	{
		// Get the camera transform.
		FVector CameraLocation;
		FRotator CameraRotation;
		GetActorEyesViewPoint(CameraLocation, CameraRotation);

		// Transform MuzzleOffset from camera space to world space.
		FVector MuzzleLocation = CameraLocation + FTransform(CameraRotation).TransformVector(mControlElementOffset);
		FRotator MuzzleRotation = CameraRotation;
		// Skew the aim to be slightly upwards.
		MuzzleRotation.Pitch += 10.0f;
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Spawn the projectile at the muzzle.
			AControlElement* Projectile = World->SpawnActor<AControlElement>(MuzzleLocation, MuzzleRotation, SpawnParams);
			if (Projectile)
			{
				// Set the projectile's initial trajectory.
				FVector LaunchDirection = MuzzleRotation.Vector();
				Projectile->Throw(LaunchDirection);

				DetachControlElement();
				SetEnableMovement(true);
				SetThrowHintVisibility(false);
			}
		}
	}
}

/**
 * Function : StartJump
 */
void AMainCharacter::StartJump()
{
	bPressedJump = true;
}

/**
 * Function : StopJump
 */
void AMainCharacter::StopJump()
{
	bPressedJump = false;
}

/**
 * Function : PressShift
 */
void AMainCharacter::PressShift()
{
	mPressedShift = true;

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("Test"));
}

/**
 * Function : ReleaseShift
 */
void AMainCharacter::ReleaseShift()
{
	mPressedShift = false;

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("Test"));
}

/**
 * Function : PressE
 */
void AMainCharacter::PressNovaSkill()
{

}

/**
 * Function : ReleaseE
 */
void AMainCharacter::ReleaseNovaSkill()
{
	const bool manaComponentIsValid = mManaComponent && mManaComponent->IsValidLowLevel();
	if (!manaComponentIsValid)
		return;

	ISkill* freezeNova = TreeSkillComponent()->GetSkill(TEXT("Freeze Nova"))->Skill();
	const float cost = freezeNova->GetCost();

	if (mManaComponent->HasEnough(cost))
	{
		freezeNova->Use(GetWorld(), this);
		mManaComponent->Spend(cost);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("I haven't got enough mana."));
	}
}

/**
 * Function : UpdateControlElementPosition
 */
void AMainCharacter::UpdateControlElementPosition()
{
	mPickedupMesh->SetWorldTransform(GetProjectileTransform());
}

/**
 * Function: RayCastToControlElement
 */
AControlElement* AMainCharacter::RayCastToControlElement()
{
	FVector start, end;

	FMinimalViewInfo viewInfo;
	mFpsCameraComponent->GetCameraView(0.0f, viewInfo);
	start = viewInfo.Location;

	end = start + GetForwardDirection() * mRayCastDistance;

	FHitResult hit;
	GetWorld()->LineTraceSingleByChannel(hit, start, end, ECollisionChannel::ECC_Visibility);

	return Cast<AControlElement>(hit.GetActor());
}

/**
 * Function : SetThrowHintVisibility
 */
void AMainCharacter::SetThrowHintVisibility(const bool visibility)
{
	mThrowHintWidget->SetVisibility(visibility ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

/**
 * Function : SetPickupHintVisibility
 */
void AMainCharacter::SetPickupHintVisibility(const bool visibility)
{
	mPickupHintWidget->SetVisibility(visibility ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

/**
 * Function : SetEnableMovement
 */
void AMainCharacter::SetEnableMovement(const bool isEnabled)
{
	if (isEnabled)
		GetMovementComponent()->Activate();
	else
		GetMovementComponent()->Deactivate();
}

/**
 * Function : Reset
 */
void AMainCharacter::Reset()
{
	mIsPickedup = true;
	mPickedupMesh->SetVisibility(true);
	SetPickupHintVisibility(false);
	SetThrowHintVisibility(true);
	SetEnableMovement(false);
}

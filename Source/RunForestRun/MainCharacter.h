// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()
class RUNFORESTRUN_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public: ///! Handlers
	// Handles input for moving forward and backward.
	void MoveForward(float value);

	// Handles input for moving right and left.
	void MoveRight(float value);

	// Handles pitch input for rotation.
	void ControlPitchInput(float value);

	// Handles input for pickup an interactive object.
	void InputPickup();

	// Pickup functionality.
	void Pickup(class AControlElement* controlElement);

	// Handles input for throw a pickuped object.
	void Throw();

	// Sets jump flag when key is pressed.
	void StartJump();

	// Clears jump flag when key is released.
	void StopJump();

	// Sets shift flat when key is pressed.
	void PressShift();

	// Clears shift flat when key is released.
	void ReleaseShift();

	void PressNovaSkill();
	void ReleaseNovaSkill();

	// Returns the control element.
	UFUNCTION(BlueprintCallable)
	class AControlElement* GetControlElement();

	// Returns forward direction.
	UFUNCTION(BlueprintCallable)
	FVector GetForwardDirection() const;

	// Returns the transform of projectile.
	UFUNCTION(BlueprintCallable)
	FTransform GetProjectileTransform() const;

	// Detaches the control element from the character.
	UFUNCTION(BlueprintCallable)
	void DetachControlElement();

	// Checks whether control element is set.
	UFUNCTION(BlueprintCallable)
	bool HasControlElement() const { return mIsPickedup; }

	// Sets visibility of the hint for throwing.
	UFUNCTION(BlueprintCallable)
	void SetThrowHintVisibility(const bool visibility);

	// Sets visibility of the hint for picking up.
	UFUNCTION(BlueprintCallable)
	void SetPickupHintVisibility(const bool visibility);

	// Enables/Disables movement of character.
	UFUNCTION(BlueprintCallable)
	void SetEnableMovement(const bool isEnabled);

	// Reset the main character.
	UFUNCTION(BlueprintCallable)
	void Reset();

	UFUNCTION(BlueprintCallable)
	class URecoveryComponent* ManaComponent() { return mManaComponent; }

	UFUNCTION(BlueprintCallable)
	class URecoveryComponent* HealthComponent() { return mHealthComponent; }

	UFUNCTION(BlueprintCallable)
	class URecoveryComponent* StaminaComponent() { return mStaminaComponent; }

	UFUNCTION(BlueprintCallable)
	class UTreeSkillComponent* TreeSkillComponent() { return mTreeSkillComponent; }

protected:
	// Creates FPS camera.
	void CreateFpsCamera();

	// Creates skeletal mesh-arms.
	void CreateFpsArms();

	// Creates picked up mesh.
	void CreatePickedupMesh();

	// Creates recovery components.
	void CreateRecoveryComponents();

	// Creates a test tree skill.
	void CreateTestSkillTree();

	// Creates the widget of hint for throwing.
	void CreateThrowHintWidget();

	// Creates the widget of hint for picking up.
	void CreatePickupHintWidget();

	// Creates the widget of UI.
	void CreateUserInterfaceWidget();

	// Spawns a control element.
	void SpawnControlElement();

	// Updates the position of the control element.
	void UpdateControlElementPosition();

	// Returns a control element as result of ray casting (null if ray casting is failed).
	class AControlElement* RayCastToControlElement();

protected:
	UPROPERTY(VisibleAnywhere)
	class UTreeSkillComponent* mTreeSkillComponent;

	UPROPERTY(EditAnywhere)
	class URecoveryComponent* mManaComponent;

	UPROPERTY(EditAnywhere)
	class URecoveryComponent* mHealthComponent;

	UPROPERTY(EditAnywhere)
	class URecoveryComponent* mStaminaComponent;

	// Indicates that the key Shift was pressed.
	UPROPERTY(VisibleAnywhere)
	bool mPressedShift = false;

	// FPS camera.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCameraComponent* mFpsCameraComponent;

	// A skeletal mesh-arms.
	UPROPERTY(VisibleDefaultsOnly)
	class USkeletalMeshComponent* mArmsSkeletalMeshComponent;

	// A control element.
	UPROPERTY(VisibleDefaultsOnly)
	class AControlElement* mControlElement;

	// Hack: Visible static mesh when a control element is picked up.
	UPROPERTY(VisibleDefaultsOnly)
	class UStaticMeshComponent* mPickedupMesh;
	bool mIsPickedup = true;

	// Control element muzzle's offset from the camera location.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector mControlElementOffset = FVector(100.0f, 0.0f, 0.0f);

	// Distance which allows to take a control element.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float mRayCastDistance = 300.0f;

	// Reference UMG asset in the editor.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> mThrowHintClass;

	// Instance of hint widget.
	UUserWidget* mThrowHintWidget;

	// Reference UMG asset in the editor.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> mPickupHintClass;

	// Instance of hint widget.
	UUserWidget* mPickupHintWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> mUserInterfaceClass;

	UUserWidget* mUserInterfaceWidget;
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "BotAIController.h"
#include "Bot.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "ControlElement.h"
#include "MainCharacter.h"
#include "RunForestRun.h"
#include "SpeedBuffComponent.h"

#include <limits>
#include <random>

namespace
{
	const FString RandomPhrases[] =
	{
		TEXT("Take Him!"),
		TEXT("My target is you!"),
		TEXT("RUN FOREST RUN"),
		TEXT("He-he"),
		TEXT("YOUR SOUL IS MINE"),
		TEXT("I feel the taste of win")
	};
}

/**
 * Function : ABotAIController
 */
ABotAIController::ABotAIController(const FObjectInitializer& ObjectIn)
	: AAIController(ObjectIn)
{
	mBlackboardComponent = ObjectIn.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));
	mBehaviorTreeComponent = ObjectIn.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorTreeComp"));
}

/**
 * Function : Possess
 */
void ABotAIController::Possess(APawn* pawn)
{
	Super::Possess(pawn);

	ABot* bot = Cast<ABot>(pawn);

	bool condition = bot && bot->GetBehavior();
	if (!condition)
		return;

	mBlackboardComponent->InitializeBlackboard(*bot->GetBehavior()->BlackboardAsset);
	mBehaviorTreeComponent->StartTree(*bot->GetBehavior());
}

/**
 * Function : FindTarget
 */
void ABotAIController::FindTarget()
{
	AMainCharacter* mainCharacter = Cast<AMainCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());

	if (mainCharacter == nullptr)
		return;

	AActor* target = nullptr;

	if (!mainCharacter->HasControlElement())
	{
		AControlElement* controlElement = mainCharacter->GetControlElement();

		ABot* bot = Cast<ABot>(GetPawn());
		// Bot picked up the control element.
		if (controlElement->HasCharacterOwner())
		{
			BuffSpeed(bot, 1.5f);
			target = mainCharacter;
			BotsDontSearchControlElement();
		}
		else
		{
			DebuffSpeed(bot);
			target = controlElement;
		}
	}

	mBlackboardComponent->SetValueAsObject(TEXT("Enemy"), target);
}

/**
 * Function : SearchForTarget
 */
void ABotAIController::SearchForTarget()
{
	APawn* bot = GetPawn();
	AMainCharacter* mainCharacter = Cast<AMainCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());

	if ((bot == nullptr) || (mainCharacter == nullptr))
	{
		GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Red, TEXT("Fuck"));
		return;
	}

	SetDestination();
}

/**
 * Function : IsClosedEnough
 */
bool ABotAIController::IsClosedEnough()
{
	ABot* bot = Cast<ABot>(GetPawn());
	AActor* target = Cast<AActor>(mBlackboardComponent->GetValueAsObject(TEXT("Enemy")));

	if (target == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("WTF?!"));
		return false;
	}

	float distance = bot->GetDistanceToInteract();
	if (Cast<AControlElement>(target))
		distance *= 0.5f;

	return FVector::Distance(bot->GetActorLocation(), target->GetActorLocation()) < bot->GetDistanceToInteract() + 50.0f;
}

/**
 * Function : InteractWithTarget
 */
void ABotAIController::InteractWithTarget()
{
	AActor* target = Cast<AActor>(mBlackboardComponent->GetValueAsObject(TEXT("Enemy")));
	if (target == nullptr)
		return;

	ABot* bot = Cast<ABot>(GetPawn());
	AMainCharacter* mainCharacter = Cast<AMainCharacter>(target);
	if (mainCharacter)
	{
		if (bot->HasControlElement())
			bot->GiveControlElementToPlayer(mainCharacter);
		else
			mainCharacter->SetEnableMovement(false);
	}
	else
	{
		AControlElement* controlElement = Cast<AControlElement>(target);
		bot->PickupControlElement(controlElement);

		WarCry();
	}
}

/**
 * Function : SetDestination
 */
void ABotAIController::SetDestination()
{
	AActor* target = Cast<AActor>(mBlackboardComponent->GetValueAsObject(TEXT("Enemy")));
	AMainCharacter* mainCharacter = Cast<AMainCharacter>(target);
	AControlElement* controlElement = Cast<AControlElement>(target);
	FVector location = GetPawn()->GetActorLocation();

	if (mainCharacter)
	{
		location = GetLocationNearEnemy(mainCharacter);
	}
	else if (controlElement)
	{
		//if (HasFreeSlotForSearchingControlElement())
		{
			location = controlElement->GetActorLocation();
			//BotSearchControlElement();
		}
		//else
		{
			//mainCharacter = Cast<AMainCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
			//location = GetLocationNearEnemy(mainCharacter, 300.0f);
			//location = 
		}
	}

	mBlackboardComponent->SetValueAsVector(TEXT("Destination"), location);
}

/**
 * Function : GetLocation
 */
FVector ABotAIController::GetLocationNearEnemy(APawn* pawn, const float additionalDistance)
{
	ABot* bot = Cast<ABot>(GetPawn());
	TArray<AActor*> bots;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABot::StaticClass(), bots);
	int32 botId = 0;
	for (int32 id = 0; id < bots.Num(); ++id)
	{
		ABot* tmpBot = Cast<ABot>(bots[id]);
		if (tmpBot == bot)
		{
			botId = id;
			break;
		}
	}

	const float pi2 = 2.0f * 3.1415f;
	const float angle = (pi2 / static_cast<float>(bots.Num())) * botId;
	const FVector offset(bot->GetDistanceToInteract() + additionalDistance, 0.0f, 0.0f);

	return pawn->GetActorLocation() + offset.RotateAngleAxis(FMath::RadiansToDegrees(angle), FVector(0.0f, 0.0f, 1.0f));
}

/**
 * Function : HasFreeSlotForSearchingControlElement
 */
bool ABotAIController::HasFreeSlotForSearchingControlElement()
{
	return mMaxBotsForControlElement > mCurrentBotsSearchControlElement;
}

/**
 * Function : BotSearchControlElement
 */
void ABotAIController::BotSearchControlElement()
{
	++mCurrentBotsSearchControlElement;
}

/**
 * Function : BotsDontSearchControlElement
 */
void ABotAIController::BotsDontSearchControlElement()
{
	mCurrentBotsSearchControlElement = 0;
}

/**
 * Function : BuffSpeed
 */
void ABotAIController::BuffSpeed(ABot* bot, const float speedFactor)
{
	if (bot->GetComponentByClass(USpeedBuffComponent::StaticClass()) != nullptr)
		return;

	USpeedBuffComponent* buffComp = NewObject<USpeedBuffComponent>(bot, TEXT("SpeedBuffComp"));
	bot->AddOwnedComponent(buffComp);

	buffComp->SetCharacterOwner(bot);
	buffComp->SetSpeedFactor(speedFactor);
	buffComp->RegisterComponentWithWorld(GetWorld());
}

/**
 * Function : DebuffSpeed
 */
void ABotAIController::DebuffSpeed(ABot* bot)
{
	USpeedBuffComponent* buffSpeed = Cast<USpeedBuffComponent>(bot->GetComponentByClass(USpeedBuffComponent::StaticClass()));
	if (buffSpeed)
		buffSpeed->DestroyComponent();
}

/**
 * Function : WarCry
 */
void ABotAIController::WarCry()
{
	std::random_device rd;
	std::default_random_engine eng(rd());
	std::uniform_int_distribution<> dis(0, _countof(RandomPhrases) - 1);

	TArray<AActor*> bots;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABot::StaticClass(), bots);

	for (int32 id = 0; id < bots.Num(); ++id)
	{
		ABot* bot = Cast<ABot>(bots[id]);
		FString warCry = FString::Printf(TEXT("Bot '%s' says: "), *bot->GetName());

		if (bot->HasControlElement())
			warCry += FString("I took it");
		else
			warCry += RandomPhrases[dis(eng)];

		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, warCry);
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunForestRunGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RUNFORESTRUN_API ARunForestRunGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	virtual void StartPlay() override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "SpeedBuffComponent.h"
#include "RunForestRun.h"

/**
 * Function : USpeedBuffComponent
 */
USpeedBuffComponent::USpeedBuffComponent()
	: UEffectComponent()
{
	//
}

/**
 * Function : ~USpeedBuffComponent
 */
USpeedBuffComponent::~USpeedBuffComponent()
{
	if (GetCharacterOwner())
		GetCharacterOwner()->GetCharacterMovement()->MaxWalkSpeed /= mSpeedFactor;
}

/**
 * Function : BeginPlay
 */
void USpeedBuffComponent::BeginPlay()
{
	GetCharacterOwner()->GetCharacterMovement()->MaxWalkSpeed *= mSpeedFactor;
}

/**
 * Function : EndPlay
 */
void USpeedBuffComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, GetOwner()->GetName());
	GetCharacterOwner()->GetCharacterMovement()->MaxWalkSpeed /= mSpeedFactor;
}

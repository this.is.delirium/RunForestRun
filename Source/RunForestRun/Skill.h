// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class RUNFORESTRUN_API ISkill
{
public:
	ISkill(const FName& name, const float cost);

	virtual void Use(UWorld* world, class ACharacter* speller) = 0;

	float GetCost() const { return mCost; }

	UFUNCTION(BlueprintCallable)
	const FName& GetName() const { return mName; }

protected:
	FName	mName;
	float	mCost;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "NodeSkill.h"
#include "TreeSkillComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RUNFORESTRUN_API UTreeSkillComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTreeSkillComponent();
	~UTreeSkillComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void AddSkill(NodeSkill* skill);
	NodeSkill* GetSkill(const FName& name);

	// Stubs
	/*float GetSkillCost(const FName& name) const;
	const FName& GetSkillName(const FName& name) const;*/

protected:
	// Fast access to the elements of skill tree.
	TMap<FName, NodeSkill*> mSkills;
};

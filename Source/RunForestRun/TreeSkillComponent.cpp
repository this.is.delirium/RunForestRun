// Fill out your copyright notice in the Description page of Project Settings.

#include "TreeSkillComponent.h"


// Sets default values for this component's properties
UTreeSkillComponent::UTreeSkillComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

/**
 * Function : ~UTreeSkillComponent
 */
UTreeSkillComponent::~UTreeSkillComponent()
{
	for (auto it = mSkills.CreateConstIterator(); it; ++it)
		delete it.Value();
}

// Called when the game starts
void UTreeSkillComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTreeSkillComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

/**
 * Function : AddSkill
 */
void UTreeSkillComponent::AddSkill(NodeSkill* skill)
{
	mSkills.Add(skill->Skill()->GetName(), skill);
}

/**
 * Function : GetSkill
 */
NodeSkill* UTreeSkillComponent::GetSkill(const FName& name)
{
	NodeSkill** finded = mSkills.Find(name);
	return (finded != nullptr) ? *finded : nullptr;
}


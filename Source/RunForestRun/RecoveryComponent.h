// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RecoveryComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RUNFORESTRUN_API URecoveryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URecoveryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable)
	void SetMaxValue(const float maxValue) { mMaxValue = maxValue; }

	UFUNCTION(BlueprintCallable)
	float GetMaxValue() const { return mMaxValue; }

	UFUNCTION(BlueprintCallable)
	void SetCurrentValue(const float currentValue) { mCurrentValue = currentValue; }

	UFUNCTION(BlueprintCallable)
	float GetCurrentValue() const { return mCurrentValue; }

	UFUNCTION(BlueprintCallable)
	bool HasEnough(const float requiredValue) { return mCurrentValue >= requiredValue; }

	UFUNCTION(BlueprintCallable)
	void Spend(const float cost) { mCurrentValue = FMath::Max(0.0f, mCurrentValue - cost); }

	void Recovery() { mCurrentValue = FMath::Min(mMaxValue, mCurrentValue + mRecoveryValue); }

	UFUNCTION(BlueprintCallable)
	void SetRecoveryValue(const float recoveryValue) { mRecoveryValue = recoveryValue; }

	UFUNCTION(BlueprintCallable)
	void EnableRecovery(const bool isEnabled) { mIsEnabledRecovery = isEnabled; }

	UFUNCTION(BlueprintCallable)
	bool IsEnabledRecovery() const { return mIsEnabledRecovery; }

	UFUNCTION(BlueprintCallable)
	void SetRecoveryCooldown(const float cooldownInSec) { mRecoveryCooldown = cooldownInSec; }

protected:
	UPROPERTY(EditAnywhere)
	float mMaxValue;

	UPROPERTY(EditAnywhere)
	float mCurrentValue;

	UPROPERTY(EditAnywhere)
	float mRecoveryValue;

	UPROPERTY(EditAnywhere)
	float mRecoveryCooldown;

	float mTimeToRecovery;

	bool mIsEnabledRecovery;
};

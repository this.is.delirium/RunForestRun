// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EffectComponent.h"
#include "FreezeDebuffComponent.generated.h"

/**
 * 
 */
UCLASS()
class RUNFORESTRUN_API UFreezeDebuffComponent : public UEffectComponent
{
	GENERATED_BODY()
	
public:
	UFreezeDebuffComponent();

	UFUNCTION(BlueprintCallable)
	void SetFreezeTime(const float time) { mFreezeTime = time; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY(EditAnywhere)
	float mFreezeTime;

	float	mCounterTime;
	bool	mIsFinished;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ControlElement.generated.h"

UCLASS()
class RUNFORESTRUN_API AControlElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AControlElement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Throws the control element in the given direction.
	UFUNCTION(BlueprintCallable)
	void Throw(const FVector& direction);

	// Sets the owner.
	UFUNCTION(BlueprintCallable)
	void SetCharacterOwner(ACharacter* characterOwner);

	// Returns the owner.
	UFUNCTION(BlueprintCallable)
	ACharacter* GetCharacterOwner() { return mCharacterOwner; }

	// Checks whether owner is set.
	UFUNCTION(BlueprintCallable)
	bool HasCharacterOwner() const { return mCharacterOwner != nullptr; }

protected:
	// Creates the sphere collision component.
	void CreateCollisionComponent();

	// Creates the projectile movement component.
	void CreateProjectileMovementComponent();

	// Creates the static mesh component.
	void CreateStaticMeshComponent();

	// Updates the position of the control element using an owner.
	//void UpdatePositionUsingOwner();

protected:
	// Sphere collision component.
	UPROPERTY(VisibleAnywhere)
	class USphereComponent* mCollisionComponent;

	// Projectile movement component.
	UPROPERTY(VisibleAnywhere)
	class UProjectileMovementComponent* mProjectileMovementComponent;

	// Static mesh component.
	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* mStaticMeshComponent;

	// Owner.
	UPROPERTY(VisibleDefaultsOnly)
	ACharacter* mCharacterOwner;
};

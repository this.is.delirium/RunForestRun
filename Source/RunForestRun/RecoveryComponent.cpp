// Fill out your copyright notice in the Description page of Project Settings.

#include "RecoveryComponent.h"


// Sets default values for this component's properties
URecoveryComponent::URecoveryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	mMaxValue			= 100.0f;
	mCurrentValue		= 0.0f;
	mRecoveryValue		= 3.0f;
	mRecoveryCooldown	= 1.0f;
	mTimeToRecovery		= 0.0f;
	mIsEnabledRecovery	= true;
}


// Called when the game starts
void URecoveryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void URecoveryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsEnabledRecovery())
	{
		mTimeToRecovery += DeltaTime;
		if (mTimeToRecovery >= mRecoveryCooldown)
		{
			Recovery();
			mTimeToRecovery -= mRecoveryCooldown;
		}
	}
}


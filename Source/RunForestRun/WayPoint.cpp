// Fill out your copyright notice in the Description page of Project Settings.

#include "WayPoint.h"
#include "RunForestRun.h"
#include "MainCharacter.h"

// Sets default values
AWayPoint::AWayPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	mCollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Waypoint"));
	RootComponent = mCollisionComponent;
}

// Called when the game starts or when spawned
void AWayPoint::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWayPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/**
 * Function : InteractWithCharacter
 */
void AWayPoint::InteractWithCharacter_Implementation(class AActor* otherActor)
{

}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Bot.generated.h"

UCLASS()
class RUNFORESTRUN_API ABot : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Returns the behavior tree.
	UFUNCTION(BlueprintCallable)
	class UBehaviorTree* GetBehavior() { return mBehavior; }

	// Returns the distance of interaction.
	float GetDistanceToInteract() const { return mDistanceToInteract; }

	// Returns the control element;
	class AControlElement* GetControlElement() { return mControlElement; }

	// Gives the control element to the specific player.
	void GiveControlElementToPlayer(class AMainCharacter* character);

	// Pickups the specific control element.
	void PickupControlElement(class AControlElement* controlElement);

	// Returns true if a bot has a control element.
	bool HasControlElement() const { return mControlElement != nullptr; }

	UFUNCTION(BlueprintCallable)
	void Enable(const bool enable);

protected:
	UPROPERTY(EditAnywhere, Category = "Behavior")
	class UBehaviorTree* mBehavior;

	UPROPERTY(EditAnywhere, Category = "Behavior")
	float mDistanceToInteract = 250.0f;

	UPROPERTY(VisibleDefaultsOnly)
	class AControlElement* mControlElement = nullptr;
};

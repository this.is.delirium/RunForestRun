// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotAIController.generated.h"

/**
 * 
 */
UCLASS()
class RUNFORESTRUN_API ABotAIController : public AAIController
{
	GENERATED_BODY()

public:
	ABotAIController(const FObjectInitializer& ObjectIn);

	virtual void Possess(class APawn* pawn) override;

	void SetDestination();

	UFUNCTION(BlueprintCallable, Category = "Behavior")
	void SearchForTarget();

	UFUNCTION(BlueprintCallable, Category = "Behavior")
	void FindTarget();

	UFUNCTION(BlueprintCallable, Category = "Behavior")
	bool IsClosedEnough();

	UFUNCTION(BlueprintCallable, Category = "Behavior")
	void InteractWithTarget();

public: // This section corresponds for buffs/debuffs. It isn't the best solution.
	void BuffSpeed(class ABot* bot, const float speedFactor);

	void DebuffSpeed(class ABot* bot);

protected:
	// Returns a location lying around the pawn.
	FVector GetLocationNearEnemy(APawn* pawn, const float additionalDistance = 0.0f);

	bool HasFreeSlotForSearchingControlElement();
	void BotSearchControlElement();
	void BotsDontSearchControlElement();

	void WarCry();

protected:
	UPROPERTY(Transient)
	class UBlackboardComponent* mBlackboardComponent;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* mBehaviorTreeComponent;

protected:
	int32 mMaxBotsForControlElement			= 3;
	int32 mCurrentBotsSearchControlElement	= 0;

	struct MarkedVector
	{
		FVector	Vector;
		bool	IsMarked;
	};

	TArray<MarkedVector> mPositionsAroundCharacter;
};

#include "FreezeNovaSkill.h"

#include "RunForestRun.h"
#include "Bot.h"

#include "FreezeDebuffComponent.h"

/**
 * Function : FreezeNovaSkill
 */
FreezeNovaSkill::FreezeNovaSkill(const FName& name, const float cost)
	: ISkill(name, cost)
{

}

/**
 * Function : Use
 */
void FreezeNovaSkill::Use(UWorld* world, class ACharacter* speller)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, TEXT("NOVA 2"));
	TArray<AActor*> bots;
	UGameplayStatics::GetAllActorsOfClass(world, ABot::StaticClass(), bots);

	for (int32 id = 0; id < bots.Num(); ++id)
	{
		ABot* bot = Cast<ABot>(bots[id]);

		UFreezeDebuffComponent* debuff = NewObject<UFreezeDebuffComponent>(bot, TEXT("FreezeDebuffComp"));
		bot->AddOwnedComponent(debuff);

		debuff->SetCharacterOwner(bot);
		debuff->RegisterComponentWithWorld(world);
	}
}
